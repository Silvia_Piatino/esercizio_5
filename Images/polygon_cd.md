@startuml


class Point {
  +Double X
  +Double Y
  ..
    +Point() { }
    +Point(const double& x, const double& y)
    +Point(const Point& point) 
    +double ComputeNorm2() const 
  ..Operator Overloading..
 +Point operator+(const Point& point) const;
 +Point operator-(const Point& point) const;
 +Point& operator-=(const Point& point);
 +Point& operator+=(const Point& point);
 +friend ostream& operator<<(ostream& stream, const Point& point)
}

interface IPolygon{
  +{abstract} double Perimeter()
  +{abstract} void AddVertex(const Point& p)
  ..Operator Overloading..
  +friend inline bool operator< (const IPolygon& lhs, const IPolygon& rhs){ return false; }
  +friend inline bool operator> (const IPolygon& lhs, const IPolygon& rhs){ return false; }
  +friend inline bool operator<=(const IPolygon& lhs, const IPolygon& rhs){ return false; }
  +friend inline bool operator>=(const IPolygon& lhs, const IPolygon& rhs){ return false; }
}

class Ellipse{
  +Point center
  +Double a
  +Double b
  --
  +Ellipse() { }
  +Ellipse(const Point& center,const double& a, const double& b) { }
  +virtual ~Ellipse() { }
  +void AddVertex(const Point& p)
  +void SetSemiAxisA(const double& a)
  +void SetSemiAxisB(const double& b)
  +double Perimeter() const;
}

class Circle{
 --
  +Circle() { }
  +Circle(const Point& center,const double& a, const double& b) { }
  +virtual ~Circle() { }
}

class Triangle{
  +vector<Point> points
  --
 +Triangle() { }
  +Triangle(const Point& p1, const Point& p2, const Point& p3);
  +~virtual Triangle()
  +void AddVertex(const Point& p)
  +double Perimeter() const;
}

class TriangleEquilateral{
  +TriangleEquilateral()
  +TriangleEquilateral(const Point& p1, const double& edge)
  +~virtual Triangle()
}

class Quadrilateral{
 +vector<Point> points;
 --
 +Quadrilateral() { }
 +Quadrilateral(const Point& p1,const Point& p2,const Point& p3, const Point& p4);
 +virtual ~Quadrilateral()
 +void AddVertex(const Point& p)
 +double Perimeter() const;
}

class Rectangle{
 +Rectangle()
 +Rectangle(const Point& p1,const Point& p2,const Point& p3,const Point& p4)
 +Rectangle(const Point& p1, const double base, const double& height)
 +virtual ~Rectangle()
}
class Square{
  +Square() { }
  +Square(const Point& p1, const Point& p2, const Point& p3, const Point& p4) {}
  +Square(const Point& p1, const double& edge) { }
  +virtual ~Square() { }
}

Point "*" --* "1" IPolygon : has
IPolygon <|.. Ellipse  : implements
IPolygon <|.. Triangle  : implements
IPolygon <|.. Quadrilateral  : implements
Ellipse <|-- Circle  : is an
Triangle <|-- TriangleEquilateral  : is a
Quadrilateral <|-- Rectangle  : is a
Rectangle <|-- Square  : is a


@enduml@startuml


class Point {
  +Double X
  +Double Y
  ..
    +Point() { }
    +Point(const double& x, const double& y)
    +Point(const Point& point) 
    +double ComputeNorm2() const 
  ..Operator Overloading..
 +Point operator+(const Point& point) const;
 +Point operator-(const Point& point) const;
 +Point& operator-=(const Point& point);
 +Point& operator+=(const Point& point);
 +friend ostream& operator<<(ostream& stream, const Point& point)
}

interface IPolygon{
  +{abstract} double Perimeter()
  +{abstract} void AddVertex(const Point& p)
  ..Operator Overloading..
  +friend inline bool operator< (const IPolygon& lhs, const IPolygon& rhs){ return false; }
  +friend inline bool operator> (const IPolygon& lhs, const IPolygon& rhs){ return false; }
  +friend inline bool operator<=(const IPolygon& lhs, const IPolygon& rhs){ return false; }
  +friend inline bool operator>=(const IPolygon& lhs, const IPolygon& rhs){ return false; }
}

class Ellipse{
  +Point center
  +Double a
  +Double b
  --
  +Ellipse() { }
  +Ellipse(const Point& center,const double& a, const double& b) { }
  +virtual ~Ellipse() { }
  +void AddVertex(const Point& p)
  +void SetSemiAxisA(const double& a)
  +void SetSemiAxisB(const double& b)
  +double Perimeter() const;
}

class Circle{
 --
  +Circle() { }
  +Circle(const Point& center,const double& a, const double& b) { }
  +virtual ~Circle() { }
}

class Triangle{
  +vector<Point> points
  --
 +Triangle() { }
  +Triangle(const Point& p1, const Point& p2, const Point& p3);
  +~virtual Triangle()
  +void AddVertex(const Point& p)
  +double Perimeter() const;
}

class TriangleEquilateral{
  +TriangleEquilateral()
  +TriangleEquilateral(const Point& p1, const double& edge)
  +~virtual Triangle()
}

class Quadrilateral{
 +vector<Point> points;
 --
 +Quadrilateral() { }
 +Quadrilateral(const Point& p1,const Point& p2,const Point& p3, const Point& p4);
 +virtual ~Quadrilateral()
 +void AddVertex(const Point& p)
 +double Perimeter() const;
}

class Rectangle{
 +Rectangle()
 +Rectangle(const Point& p1,const Point& p2,const Point& p3,const Point& p4)
 +Rectangle(const Point& p1, const double base, const double& height)
 +virtual ~Rectangle()
}
class Square{
  +Square() { }
  +Square(const Point& p1, const Point& p2, const Point& p3, const Point& p4) {}
  +Square(const Point& p1, const double& edge) { }
  +virtual ~Square() { }
}

Point "*" --* "1" IPolygon : has
IPolygon <|.. Ellipse  : implements
IPolygon <|.. Triangle  : implements
IPolygon <|.. Quadrilateral  : implements
Ellipse <|-- Circle  : is an
Triangle <|-- TriangleEquilateral  : is a
Quadrilateral <|-- Rectangle  : is a
Rectangle <|-- Square  : is a


@enduml
