#include "shape.h"
#include <math.h>
#include <cmath>

namespace ShapeLibrary {
   Point::Point(const double& x, const double& y){
       X = x;
       Y = y;
   }

   Point::Point(const Point& point){
       *this=point;
       //this->X = point.X;
       //this->Y = point.Y;
   }
   double Point::ComputeNorm2() const{
       return sqrt(X*X+Y*Y);
   }

   Ellipse::Ellipse(const Point& center, const double& a, const double& b){
       this->_center=center; // qui lo posso fare perché ho un default constructor
       this->_a = a;
       this->_b = b;
   }

  double Ellipse::Perimeter() const
  {
      return 2*M_PI*sqrt((_a*_a+_b*_b)/2);
  }

  Circle::Circle(const Point& center, const double& radius): Ellipse(center, radius, radius)
   {}

  Triangle::Triangle(const Point& p1, const Point& p2, const Point& p3)
  {
    points.push_back(p1);
    points.push_back(p2);
    points.push_back(p3);
  }

  void Triangle::AddVertex(const Point &point){
      if(points.size()>3)
          throw runtime_error("The triangle has already three points");
      points.push_back(point);
  }

  double Triangle::Perimeter() const
  {
    double perimeter = 0;
    int numPoints = points.size();
    for(int i=0; i<numPoints; i++){
        perimeter+=Point(points[(i+1)%numPoints]-points[i]).ComputeNorm2();
    }
    return perimeter;
  }

  TriangleEquilateral::TriangleEquilateral(const Point& p1, const double& edge): Triangle(p1, Point(p1.X+edge, p1.Y), Point(p1.X+edge/2, p1.Y+sqrt(3)*edge/2))
  {
    cout <<"creato triangolo equilatero con npunti: "<< points.size()<<endl;
  }

  Quadrilateral::Quadrilateral(const Point& p1,
                               const Point& p2,
                               const Point& p3,
                               const Point& p4)
  {
      points.push_back(p1);
      points.push_back(p2);
      points.push_back(p3);
      points.push_back(p4);
  }

  void Quadrilateral::AddVertex(const Point &p){
      if(points.size()>4)
          throw runtime_error("The quadrilateral has already four points");
      points.push_back(p);
  }

  double Quadrilateral::Perimeter() const
  {
    double perimeter = 0;
    int numPoints = points.size();
    for(int i=0; i<numPoints; i++){
        perimeter+=Point(points[(i+1)%numPoints]-points[i]).ComputeNorm2();
    }
    return perimeter;
  }

  Rectangle::Rectangle(const Point& p1, const Point& p2, const Point& p3, const Point& p4){
      points.push_back(p1);
      points.push_back(p2);
      points.push_back(p3);
      points.push_back(p4);
  }

  Rectangle::Rectangle(const Point& p1,
                       const double& base,
                       const double& height): Quadrilateral(p1, Point(p1.X + base, p1.Y), Point(p1.X + base, p1.Y + height), Point(p1.X, p1.Y + height))
  {}

  Square::Square(const Point& p1, const Point& p2, const Point& p3, const Point& p4){
      points.push_back(p1);
      points.push_back(p2);
      points.push_back(p3);
      points.push_back(p4);
  }

  Square::Square(const Point& p1, const double& edge): Rectangle(p1, edge, edge)
  {}

  Point Point::operator+(const Point& point) const
  {
    Point newPoint = Point(X, Y);
    newPoint.X = X + point.X;
    newPoint.Y = Y + point.Y;
    return newPoint;
  }

  Point Point::operator-(const Point& point) const
  {
      Point newPoint(X, Y);
      newPoint.X = X - point.X;
      newPoint.Y = Y - point.Y;
      return newPoint;
  }

  Point&Point::operator-=(const Point& point)
  {
    X -= point.X;
    Y -= point.Y;
    return *this;
  }

  Point&Point::operator+=(const Point& point)
  {
      X += point.X;
      Y += point.Y;
      return *this;
  }
}
