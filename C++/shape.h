#ifndef SHAPE_H
#define SHAPE_H

#include <iostream>
#include <vector>
#include <math.h>
using namespace std;

namespace ShapeLibrary {

  class Point {
    public:
      double X;
      double Y;
      Point() { X=0.0; Y=0.0; }
      Point(const double& x,
            const double& y);
      Point(const Point& point);

      double ComputeNorm2() const;
      Point& operator=(const Point& point) = default;
      Point operator+(const Point& point) const;
      Point operator-(const Point& point) const;
      Point& operator-=(const Point& point);
      Point& operator+=(const Point& point);
      friend ostream& operator<<(ostream& stream, const Point& point)
      {
        stream << "Point: x = " << point.X << " y = " << point.Y << endl;
        return stream;
      }
  };

  class IPolygon {
    public:
      virtual double Perimeter() const = 0;
      virtual void AddVertex(const Point& point) = 0;
      friend inline bool operator< (const IPolygon& lhs, const IPolygon& rhs){ return lhs.Perimeter() < rhs.Perimeter(); } // i poligoni possono usare questa funzione
      friend inline bool operator> (const IPolygon& lhs, const IPolygon& rhs){ return lhs.Perimeter() > rhs.Perimeter(); }
      friend inline bool operator<=(const IPolygon& lhs, const IPolygon& rhs){ return lhs.Perimeter() <= rhs.Perimeter(); }
      friend inline bool operator>=(const IPolygon& lhs, const IPolygon& rhs){ return lhs.Perimeter() >= rhs.Perimeter(); }

  };

  class Ellipse : public IPolygon
  {
    protected:
      Point _center;
      double _a;
      double _b;

    public:
      Ellipse() { _center=Point(0, 0); _a=0; _b=0; }
      Ellipse(const Point& center,
              const double& a,
              const double& b);
      virtual ~Ellipse() { }
      void AddVertex(const Point& point) { throw runtime_error("Function not implemented"); } //non ha senso su un'ellisse
      double Perimeter() const;
  };

  class Circle : public Ellipse
  {
    public:
      Circle() { _center=Point(0, 0); _a=0; _b=0;}
      Circle(const Point& center,
             const double& radius);
      virtual ~Circle() { }
  };


  class Triangle : public IPolygon
  {
    protected:
      vector<Point> points;

    public:
      Triangle() { }
      Triangle(const Point& p1,
               const Point& p2,
               const Point& p3);

      void AddVertex(const Point& point);
      double Perimeter() const;
  };


  class TriangleEquilateral : public Triangle
  {
    public:
      TriangleEquilateral(const Point& p1,
                          const double& edge);
  };

  class Quadrilateral : public IPolygon
  {
    protected:
      vector<Point> points;

    public:
      Quadrilateral() { }
      Quadrilateral(const Point& p1,
                    const Point& p2,
                    const Point& p3,
                    const Point& p4);
      virtual ~Quadrilateral() { }
      void AddVertex(const Point& p);

      double Perimeter() const;
  };

  class Rectangle : public Quadrilateral
  {
    public:
      Rectangle() { }
      Rectangle(const Point& p1,
                const Point& p2,
                const Point& p3,
                const Point& p4);
      Rectangle(const Point& p1,
                const double& base,
                const double& height);
      virtual ~Rectangle() { }
  };

  class Square: public Rectangle
  {
    public:
      Square() { }
      Square(const Point& p1,
             const Point& p2,
             const Point& p3,
             const Point& p4);
      Square(const Point& p1,
             const double& edge);
      virtual ~Square() { }
  };
}

#endif // SHAPE_H
